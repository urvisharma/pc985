#include<stdio.h>
int input()
{
  int n;
  printf("enter the value");
  scanf("%d",&n);
  return n;
}
int swap(int *a ,int *b)
{
  int temp;
  temp=*a;
  *a=*b;
  *b=temp;
}
void output(int a ,int b)
{
  printf("the swapped value is %d,%d",a,b);
}
int main()
{
  int a,b;
  a=input();
  b=input();
  swap(&a,&b);
  output(a,b);
  return 0;
}

  
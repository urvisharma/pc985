#include<stdio.h>
int input_n()
{
  int n;
  printf("enter the value of n");
  scanf("%d",&n);
  return n;
}
int input_arr(int n,int arr[n])
{
  int i;
  printf("enter the array elements");
  for(i=0;i<n;i++)
  {
    scanf("%d",&arr[i]);
  }
}
int calculate(int n,int arr[n],int *sum)
{
  int i;
  for(i=0;i<n;i++)
  {
    *sum+=arr[i];
  }

}
void output(int n,int arr[n],int sum)
{
  printf("sum=%d",sum);
}
int main()
{
  int n,i,sum;
  n=input_n();
  int arr[n];
  input_arr(n,arr);
  calculate(n,arr,&sum);
  output(n,arr,sum);
  return 0;
}


#include<stdio.h>
#include<math.h>
struct point
{
  float x;
  float y;
};
typedef struct point Point;
Point input_point()
{
  Point p;
  printf("enter a number");
  scanf("%f%f",&p.x,&p.y);
  return p;
}
float distance(Point p1,Point p2)
{
  float distance;
  distance=sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
  return distance;
}
void output(Point p1,Point p2,float distance)
{
  printf("the distance between two points %f%f and %f%f is %4f",p1.x,p1.y,p2.x,p2.y,distance);
}
int main()
{
  float result;
  Point p1,p2;
  p1=input_point();
  p2=input_point();
  result=distance(p1,p2);
  output(p1,p2,result);
  return 0;
}
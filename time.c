/*function with return value and with parameter*/
/*c pgm to convert time to minutes using functions*/
#include<stdio.h>
int compute_time(int hr,int min)
{
  int time;
  time=hr*60+min;
  return(time);
}
int main()
{
  int h,m,t1;
  printf("enter hrs and min");
  scanf("%d%d",&h,&m);
  t1=compute_time(h,m);
  printf("%d",t1);
  return 0;
}  
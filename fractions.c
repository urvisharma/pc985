#include<stdio.h>
int input_af()
{
  int x1,y1,*px1=&x1,*py1=&y1;
  printf("enter the first fraction");
  scanf("%d%d",&x1,&y1);
}
int input_bf()
{
  int x2,y2,*px2=&x2,*py2=&y2;
  printf("enter the second fraction");
  scanf("%d%d",&x2,&y2);
}
int calculate(int *px1,int *px2,int *py1,int *py2,int *px3,int *py3)
{
  *px3=(*px1**py2)+(*px2**py1);
  *py3=*py1**py2;
}
int simplify(int *px1,int *px2,int *py1,int *py2,int *px3,int *py3)
{
  int div,i;
  if(*px3>*py3)
  {
    div=*py3;
  }
  else 
  {
    div=*px3;
  }
  for(i=div;i>0;i--)
  {
    if(*px3%i==0&&*py3%i==0)
    {
      *px3=*px3/i;
      *py3=*py3/i;
    }
   }
}
int output(int *px1,int *px2,int *py1,int *py2,int *px3,int *py3)
{
  printf("sum of two fractions is %d %d",*px3,*py3);
}
int main()
{
  int  x1,y1,*px1=&x1,*py1=&y1,x2,y2,*px2=&x2,*py2=&y2,x3,y3,*px3=&x3,*py3=&y3;
  *px1=input_af();
  *px2=input_bf();
  calculate(&x1,&x2,&y1,&y2,&x3,&y3);
  simplify(&x1,&x2,&y1,&y2,&x3,&y3);
  output(&x1,&x2,&y1,&y2,&x3,&y3);
  return 0;
}
  



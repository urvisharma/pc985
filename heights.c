#include<stdio.h>
struct heights
{
  int ft;
  int inch;
};
int input_n()
{
  int n;
  printf("enter the value of n");
  scanf("%d",&n);
  return n;
}
int input_no(int n,struct heights HIGH[n])
{
  int i;
  for(i=0;i<n;i++)
  {
    printf("enter the ft and inch");
    scanf("%d%d",&HIGH[i].ft,&HIGH[i].inch);
  }
}
struct heights add_heights(int n,struct heights HIGH[n],struct heights *sum)
{
  int i;
  for(i=0;i<n;i++)
  {
    (*sum).inch+=HIGH[i].inch;
    (*sum).ft+=HIGH[i].ft;
  }
}
void output(int n,struct heights HIGH[n],struct heights sum)
{
  printf("the two heights are %d ft %d inch",sum.ft,sum.inch);
}
int main()
{
  int n;
  n=input_n();
  struct heights HIGH[n],sum;
  input_no(n,HIGH);
  add_heights(n,HIGH,&sum);
  output(n,HIGH,sum);
  return 0; 
}
  
 
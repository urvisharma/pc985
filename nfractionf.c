#include<stdio.h>
struct fraction
{
  int new;
  int deno;
};
int input_n()
{
  int n;
  printf("enter the value of n");
  scanf("%d",&n);
  return n;
}
int input_no(int n,struct fraction fract[n])
{
  int i;
  for(i=0;i<n;i++)
  {
    printf("enter nemerator and denominator");
    scanf("%d%d",&fract[i].new,&fract[i].deno);
  }
}
struct fraction calculate(int n,struct fraction fract[n],struct fraction *sum)
{
  int i,l,r;
  sum->deno=fract[0].deno;
  for(i=0;i<n;i++)
  {
    sum->deno=sum->deno*fract[i].deno;
  }
  for(i=0;i<n;i++)
  {
    l=(fract[i].new*sum->deno)/fract[i].deno;
    sum->new=sum->new+l;
  }
  
}
int gcd(int a,int b)
{  
  while(a!=b)
  {
    if(a>b)
    a=a-b;
    else
    b=b-a;
  }
  return a;
}
int simplify(struct fraction *sum,int a)
{
  sum->new=sum->new/a;
  sum->deno=sum->deno/a;
}
  
void output(struct fraction sum)
{
  printf("the sum of fractions is %d/%d ",sum.new,sum.deno);
} 
int main()
{
  int n,g;
  n=input_n();
  struct fraction fract[n],sum;
  input_no(n,fract);
  calculate(n,fract,&sum);
  g=gcd(sum.new,sum.deno);
  simplify(&sum,g);
  output(sum);
  return 0; 
}  
  

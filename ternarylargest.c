#include<stdio.h>
int input()
{
  int a;
  printf("enter the value");
  scanf("%d",&a);
  return a;
}
int compute_largest(int a,int b,int c)
{
  int l;
  l=a>b?(a>c?a:c):(b>c?b:c);
  return l;
}
int output(int a,int b,int c,int largest)
{
  printf("maximum value of 3 numbers %d,%d,%d is %d",a,b,c,largest);
}
int main()
{
  int a,b,c,result;
  a=input();
  b=input();
  c=input();
  result=compute_largest(a,b,c);
  output(a,b,c,result);
  return 0;